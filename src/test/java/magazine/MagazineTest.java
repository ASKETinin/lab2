package magazine;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class MagazineTest {

    private Magazine magazine;
    private Article article1;
    private Article article2;
    private Section section1;
    private Section section2;
    private Author author1;
    private Author author2;
    private Editor editor1;
    private Department department1;

    @BeforeEach
    public void setUp() {
        magazine = new Magazine("Test Magazine");
        section1 = new Section(magazine, "Section 1");
        section2 = new Section(magazine, "Section 2");
        author1 = new Author(magazine);
        author2 = new Author(magazine);
        department1 = new Department(magazine, "Department 1");
        editor1 = new Editor(magazine, department1);
        article1 = new Article(author1, "Article 1");
        article2 = new Article(author2, "Article 2");

        magazine.addSection(section1);
        magazine.addSection(section2);
        magazine.addAuthor(author1);
        magazine.addAuthor(author2);
        magazine.addDepartment(department1);
        magazine.addEditor(editor1);
        magazine.addArticle(article1);
        magazine.addArticle(article2);
    }

    @Test
    public void testAddArticle() {
        magazine.addArticle(article1);
        magazine.addArticle(article2);

        assertTrue(magazine.getArticles().contains(article1));
        assertTrue(magazine.getArticles().contains(article2));
    }

    @Test
    public void testGetSections() {
        assertTrue(magazine.getSections().contains(section1));
        assertTrue(magazine.getSections().contains(section2));
    }

    @Test
    public void testGetArticlesOfSection() {
        article1.addSection(section1);
        article2.addSection(section2);

        assertEquals(1, magazine.getArticlesOfSection(section1).size());
        assertEquals(1, magazine.getArticlesOfSection(section2).size());
    }

    @Test
    public void testGetAverageTimeSpentOnArticle() {
        article1.setHoursSpentOnWriting(5.0f);
        article2.setHoursSpentOnWriting(7.0f);

        float expectedAverage = (5.0f + 7.0f) / 2;
        assertEquals(expectedAverage, magazine.getAverageTimeSpentOnArticle(), 0.01);
    }

    @Test
    public void testGetSectionWithFewestArticles() {
        Section section3 = new Section(magazine, "Section 3");

        article1.addSection(section1);
        article1.addSection(section2);
        article2.addSection(section1);
        article2.addSection(section2);
        article1.addSection(section3);

        assertEquals(section3, magazine.getSectionWithFewestArticles());
    }

    @Test
    public void testGetArticleWithLargestTextVolume() {
        article1.setTextVolume(500);
        article2.setTextVolume(700);

        assertEquals(article2, magazine.getArticleWithLargestTextVolume());
    }
}