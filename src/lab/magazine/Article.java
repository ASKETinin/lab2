package magazine;

import java.util.Date;
import java.util.List;
import java.util.ArrayList;


public class Article {
    private Author author;
    private Editor editor;
    private ArticleState state;
    private String title;
    private Integer textVolume;
    private Language language;
    private ArrayList<Section> sections = new ArrayList<>();
    private Float hoursSpentOnWriting;
    private Date submissionDate;
    private Date publicationDate;

    public Article(Author author, String title) {
        this.author = author;
        this.title = title;
    }

    private List<ArticleObserver> observers = new ArrayList<>();

    public void addObserver(ArticleObserver observer) {
        observers.add(observer);
    }

    public void setState(ArticleState state) {
        // Зміна стану статті
        notifyObservers();
    }

    private void notifyObservers() {
        for (ArticleObserver observer : observers) {
            observer.articleStateChanged(this);
        }
    }

    public Author getAuthor() {
        return this.author;
    }

    public Editor getEditor() {
        return this.editor;
    }

    protected void setEditor(Editor editor) {
        this.editor = editor;
    }

    public ArticleState getState() {
        return this.state;
    }

    public String getTitle() {
        return this.title;
    }

    protected void setTitle(String title) {
        this.title = title;
    }

    public Integer getTextVolume() {
        return this.textVolume;
    }

    protected void setTextVolume(Integer textVolume) {
        this.textVolume = textVolume;
    }

    public Language getLanguage() {
        return this.language;
    }

    protected void setLanguage(Language language) {
        this.language = language;
    }

    public ArrayList<Section> getSections() {
        return this.sections;
    }

    protected void addSection(Section section) {
        this.sections.add(section);
    }

    public Float getHoursSpentOnWriting() {
        return this.hoursSpentOnWriting;
    }

    protected void setHoursSpentOnWriting(Float time) {
        this.hoursSpentOnWriting = time;
    }

    public String toString() {
        String articleInformation = String.format("Title: %s\nHours spent on writing: %s\n", this.title,
                this.hoursSpentOnWriting);
        return articleInformation;
    }

    protected void publish() {
        this.state = ArticleState.PUBLISHED;
    }
}
