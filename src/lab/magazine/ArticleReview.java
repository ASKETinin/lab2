package magazine;

public enum ArticleReview {
    EXCLUDE,
    EDIT,
    PUBLISH
}
