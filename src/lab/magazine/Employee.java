package magazine;

public abstract class Employee {
    protected Magazine magazine;
    protected String firstName;
    protected String lastName;
    protected String middleName = "";
    protected String email;
    protected String phoneNumber;

    public Employee(Magazine magazine) {
        this.magazine = magazine;
    }

    public Magazine getMagazine() {
        return this.magazine;
    }

    public String getFullName() {
        String fullName = this.firstName;
        if (this.middleName != null) {
            fullName += this.middleName;
        }
        fullName += this.lastName;
        return fullName;
    }

    public void setFullName(String first, String last) {
        this.firstName = first;
        this.lastName = last;
    }

    public void setFullName(String first, String middle, String last) {
        this.firstName = first;
        this.middleName = middle;
        this.lastName = last;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return this.phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String toString() {
        String fullEmployeeInformation = String.format(
                "Magazine: '%s'\nFull name: %s\nEmail: %s\nPhone number: %s\n",
                this.magazine.getName(),
                this.getFullName(),
                this.email,
                this.phoneNumber);
        return fullEmployeeInformation;
    }
}
