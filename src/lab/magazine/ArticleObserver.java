package magazine;

public interface ArticleObserver {
    void articleStateChanged(Article article);
}
