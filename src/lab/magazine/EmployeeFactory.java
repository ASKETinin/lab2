package magazine;

public class EmployeeFactory {
    public static Author createAuthor(Magazine magazine) {
        Author author = new Author(magazine);
        magazine.addAuthor(author);
        return author;
    }

    public static Editor createEditor(Magazine magazine, Department department) {
        Editor editor = new Editor(magazine, department);
        magazine.addEditor(editor);
        return editor;
    }
    // Інші методи create для різних типів співробітників
}