package magazine;


public enum ArticleState {
    CANCELED,
    IN_PROGRESS,
    REVIEW,
    EDITED,
    PUBLISHED
}