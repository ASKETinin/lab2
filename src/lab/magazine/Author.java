package magazine;

public class Author extends Employee {
    public Author(Magazine magazine) {
        super(magazine);
        this.magazine.addAuthor(this);
    }
}
