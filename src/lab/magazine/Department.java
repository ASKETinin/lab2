package magazine;

public class Department {
    private Magazine magazine;
    private String name;

    public Department(Magazine magazine, String name) {
        this.magazine = magazine;
        this.magazine.addDepartment(this);
        this.setName(name);
    }

    public Magazine getMagazine() {
        return this.magazine;
    }

    public String getName() {
        return this.name;
    }

    protected void setName(String name) {
        this.name = name;
    }

    public String toString() {
        String departmentInformation = String.format("Department name: '%s'\n", this.getName());
        return departmentInformation;
    }
}
