package magazine;

public class Editor extends Employee {
    private Department department;

    public Editor(Magazine magazine, Department department) {
        super(magazine);
        this.setDepartment(department);
        this.magazine.addEditor(this);
    }

    public Department getDepartment() {
        return this.department;
    }

    protected void setDepartment(Department department) {
        this.department = department;
    }

    public String toString() {
        String fullEditorInformation = super.toString() + String.format("Department: %s\n", this.department);
        return fullEditorInformation;
    }

    public void checkArticle(Article article, ArticleReview status) {
        article.setEditor(this);
        switch (status) {
            case PUBLISH -> article.publish();
            case EDIT -> article.setState(ArticleState.EDITED);
            case EXCLUDE -> article.setState(ArticleState.CANCELED);
        }
    }
}
