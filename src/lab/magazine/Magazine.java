package magazine;

import java.util.ArrayList;
import java.util.NoSuchElementException;

public class Magazine {
    private String name;
    private ArrayList<Article> articles = new ArrayList<>();
    private ArrayList<Editor> editors = new ArrayList<>();
    private ArrayList<Department> departments = new ArrayList<>();
    private ArrayList<Author> authors = new ArrayList<>();
    private ArrayList<Section> sections = new ArrayList<>();

    // Приватний статичний екземпляр
    private static Magazine instance;

    // Приватний конструктор для запобігання створенню інших екземплярів
    public Magazine(String name) {
        this.name = name;
    }

    // Публічний метод для отримання екземпляра
    public static Magazine getInstance(String name) {
        if (instance == null) {
            instance = new Magazine(name);
        }
        return instance;
    }

    public String getName() {
        return this.name;
    }

    public ArrayList<Article> getArticles() {
        return this.articles;
    }

    protected void addArticle(Article article) {
        this.articles.add(article);
    }

    public ArrayList<Article> getArticlesOfEditor(Editor editor) {
        ArrayList<Article> articles = new ArrayList<>();
        for (Article article : this.getArticles()) {
            if (article.getEditor() == editor) {
                articles.add(article);
            }
        }
        return articles;
    }

    public ArrayList<Article> getArticlesOfAuthor(Author author) {
        ArrayList<Article> articles = new ArrayList<>();
        for (Article article : this.getArticles()) {
            if (article.getAuthor() == author) {
                articles.add(article);
            }
        }
        return articles;
    }

    public ArrayList<Article> getArticlesOfSection(Section section) {
        ArrayList<Article> articles = new ArrayList<>();
        for (Article article : this.getArticles()) {
            if (article.getSections().contains(section)) {
                articles.add(article);
            }
        }
        return articles;
    }

    public ArrayList<Editor> getEditors() {
        return this.editors;
    }

    protected void addEditor(Editor editor) {
        this.editors.add(editor);
    }

    public ArrayList<Department> getDepartments() {
        return this.departments;
    }

    protected void addDepartment(Department department) {
        this.departments.add(department);
    }

    public ArrayList<Editor> getEditorsOfDepartment(Department department) {
        ArrayList<Editor> editors = new ArrayList<>();
        for (Editor editor : this.getEditors()) {
            if (editor.getDepartment() == department) {
                editors.add(editor);
            }
        }
        return editors;
    }

    public ArrayList<Author> getAuthors() {
        return this.authors;
    }

    protected void addAuthor(Author author) {
        this.authors.add(author);
    }

    public ArrayList<Section> getSections() {
        return this.sections;
    }

    protected void addSection(Section section) {
        this.sections.add(section);
    }

    public String toString() {
        return this.getName();
    } //   TODO finish method}

    public Float getAverageTimeSpentOnArticle() {
        Float summ = 0f;
        int counter = 0;
        for (Article article : this.getArticles()) {
            Float time = article.getHoursSpentOnWriting();
            if (time != null) {
                summ += time;
                counter += 1;
            }
        }
        Float average = 0f;
        if (counter > 0) {
            average = summ / counter;
        }
        return average;
    }

    public Section getSectionWithFewestArticles() {
        if (this.sections.size() < 1) {
            throw new NoSuchElementException("There is no sections in this magazine");
        }
        Section neededSection = this.getSections().get(0);
        int minNumberOfArticles = this.getArticlesOfSection(neededSection).size();
        for (Section section : this.getSections()) {
            int numberOfArticles = this.getArticlesOfSection(section).size();
            if (numberOfArticles < minNumberOfArticles) {
                neededSection = section;
                minNumberOfArticles = numberOfArticles;
            }
        }
        return neededSection;
    }

    public Article getArticleWithLargestTextVolume() {
        if (this.articles.size() < 1) {
            throw new NoSuchElementException("There is no articles in this magazine");
        }
        Article neededArticle = this.getArticles().get(0);
        int maxTextVolume = neededArticle.getTextVolume();

        for (Article article : this.getArticles()) {
            Integer textVolume = article.getTextVolume();

            if (textVolume > maxTextVolume) {
                neededArticle = article;
                maxTextVolume = textVolume;
            }
        }

        return neededArticle;
    }
}
