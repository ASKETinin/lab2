package magazine;


public class Section {
    private Magazine magazine;
    private String name;
    private String description;

    public Section(Magazine magazine, String name) {
        this.magazine = magazine;
        this.magazine.addSection(this);
        this.name = name;
    }

    public Magazine getMagazine() {
        return this.magazine;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String toString() {
        String sectionInformation = String.format("Name: '%s'\nDescription: %s\n", this.getName(),
                this.getDescription());
        return sectionInformation;
    }
}
